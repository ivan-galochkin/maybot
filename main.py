import requests
import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
from vk_api.utils import get_random_id
import pymorphy2
import random

vk_session = vk_api.VkApi(token='20e53d2ae5ac7d7ce9b5dc4970b723f41673ff7ec352170b93ad81e8fa40855c8c3f48a87196143c55a1d')
vk = vk_session.get_api()

longpoll = VkBotLongPoll(vk_session, 207375488)

morph = pymorphy2.MorphAnalyzer()


def prepare_context(context, answer):
    return f'"{context}?" – {answer}'


def send_message(chat_id, text_message):
    vk.messages.send(random_id=get_random_id(),
                     chat_id=chat_id,
                     message=text_message)


def who(chat_id):
    users = []
    dictionary_with_user_data = vk.messages.getConversationMembers(peer_id=2000000000 + chat_id)
    for user in dictionary_with_user_data['items']:
        if str(user['member_id'])[0] != '-':
            users.append(int(user['member_id']))
    random_user = random.choice(users)
    user_get = vk.users.get(user_ids=(random_user))
    user_get = user_get[0]
    first_name = user_get['first_name']
    last_name = user_get['last_name']
    return [first_name, last_name]


def inflect_name(names, rule=""):
    first_name = morph.parse(names[0])[0]
    last_name = morph.parse(names[1])[0]
    if rule:
        first_name = first_name.inflect({rule})
        last_name = last_name.inflect({rule})
    else:
        first_name = first_name.inflect({base_word.tag.case})
        last_name = last_name.inflect({base_word.tag.case})

    return first_name.word.capitalize() + " " + last_name.word.capitalize()


while True:
    try:
        for event in longpoll.listen():
            if event.type == VkBotEventType.MESSAGE_NEW or event.type == VkEventType.MESSAGE_NEW:
                message = event.object["message"]["text"]
                message_list = message.split(' ')
                first_word = message_list[0].lower()
                second_word = message_list[1]
                message_list.pop(0)
                if first_word == "мэй" or first_word == "мей":
                    base_word = morph.parse(second_word)[0]
                    context = " ".join(message_list)
                    if base_word.tag.POS == "NPRO":
                        send_message(event.chat_id, prepare_context(context, inflect_name(who(event.chat_id))))
                    elif base_word.tag.POS == "ADJF":
                        send_message(event.chat_id, prepare_context(context, inflect_name(who(event.chat_id), "gent")))
    except BaseException as exc:
        print(exc)
        pass
